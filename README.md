# Unstoppable GLP Autocompounder

A free to use ERC4626 based Autocompounder vault for https://gmx.io/ 's GLP token.



## Release Article 

https://medium.com/@unstoppablefinance/zero-fees-free-forever-75d1bcbcf41f


## dApp
https://glp.unstoppablefinance.org

## Contract Addresses Arbitrum:

| Contract | Address |
| --- | ---|
| Vault| `0xfF6b69B78DF465bf7e55D242fD11456158D1600A`|
|Strategy| `0xaF51BA9524a5cd9C4Ff998608Bd5f1E1C8c855C9`|


## Audits
Internal Audit: [UF_Audit-Report_GLP-Autocompounder.pdf](audits/UF_Audit-Report_GLP-Autocompounder.pdf)

## License
SPDX-License-Identifier: AGPL-3.0-or-later


### Learn more
https://unstoppablefinance.org/


